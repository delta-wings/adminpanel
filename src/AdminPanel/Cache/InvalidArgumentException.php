<?php

namespace AdminPanel\Cache;

class InvalidArgumentException extends CacheException implements \Psr\SimpleCache\InvalidArgumentException
{
}
