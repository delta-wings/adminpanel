<?php

namespace AdminPanel\Cache;

use Exception;

class CacheException extends Exception implements \Psr\SimpleCache\CacheException
{
}
